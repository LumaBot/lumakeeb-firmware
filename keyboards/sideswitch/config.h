#pragma once

#include "config_common.h"

/* USB Device descriptor parameter */
#define VENDOR_ID       0xEEEE
#define PRODUCT_ID      0x2023
#define DEVICE_VER		0x0001
#define MANUFACTURER    LumaBot
#define PRODUCT         Sideswitch

/* key matrix size */
#define MATRIX_ROWS 1
#define MATRIX_COLS 12

/* key matrix pins */
#define MATRIX_ROW_PINS { D1 }
#define MATRIX_COL_PINS { B2, B1, B3, B4, B5, B6, F7, F6, E6, D7, D4, C6 }

/* COL2ROW or ROW2COL */
#define DIODE_DIRECTION COL2ROW

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5
#define VIAL_KEYBOARD_UID { 0x78, 0x4F, 0x78, 0xC5, 0x16, 0x1E, 0x27, 0xFA }
#define VIAL_UNLOCK_COMBO_ROWS { 0, 0 }
#define VIAL_UNLOCK_COMBO_COLS { 10, 4 }
