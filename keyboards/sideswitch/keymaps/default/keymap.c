#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {[0] = LAYOUT_h(
    KC_INS, KC_MUTE, KC_PSCR, KC_END, KC_PGDN, KC_HOME, KC_MPRV, KC_MPLY, KC_DEL, KC_SLCK, KC_PGUP, KC_MNXT
    )
};

#if defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][2] = {
    [0] = { ENCODER_CCW_CW( KC_VOLD, KC_VOLU ) }
    //                  Encoder 1
};
#endif
