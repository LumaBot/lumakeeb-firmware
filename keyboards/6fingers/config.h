#pragma once

#include "config_common.h"

/* USB Device descriptor parameter */
#define VENDOR_ID       0xEEEE
#define PRODUCT_ID      0x2023
#define DEVICE_VER		0x0001
#define MANUFACTURER    LumaBot
#define PRODUCT         Six Fingers MacroPad

/* key matrix size */
#define MATRIX_ROWS 1
#define MATRIX_COLS 6

/* key matrix pins */
#define MATRIX_ROW_PINS { D1 }
#define MATRIX_COL_PINS { F6, F7, B3, B2, F5, B1 }

/* COL2ROW or ROW2COL */
#define DIODE_DIRECTION COL2ROW

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5
#define VIAL_KEYBOARD_UID { 0x0C, 0x4E, 0x9A, 0xB9, 0xEC, 0x5D, 0xDD, 0x96 }
#define VIAL_UNLOCK_COMBO_ROWS { 0, 0 }
#define VIAL_UNLOCK_COMBO_COLS { 0, 2 }
