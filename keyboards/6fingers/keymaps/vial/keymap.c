#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {[0] = LAYOUT_h(
    KC_MPRV, KC_MUTE, KC_MNXT, KC_MPLY, KC_VOLD, KC_VOLU
    )
};
